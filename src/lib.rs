//! plantd crate
//!
//! Service, client, worker, and messaging types for working with plantd.

#![deny(
    //missing_docs,
    missing_copy_implementations,
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]
#![cfg_attr(feature = "dev", allow(unstable_features))]
#![cfg_attr(feature = "dev", feature(plugin))]
#![cfg_attr(feature = "dev", plugin(clippy))]

extern crate time;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate iota;
extern crate czmq;

/// plantd base module
pub mod plantd {
    /// Returns the version string
    pub fn version() -> &'static str {
        let ver: &'static str = env!("CARGO_PKG_VERSION");
        ver
    }
}

use crate::plantd::version;

/// Print library version
pub fn print_version() {
    println!("{}", version());
}

pub mod mdp;
