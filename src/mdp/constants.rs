//! Module defining constants relevant to the MDP API.

/// Version of MDP/Client we implement
pub static MDPC_CLIENT: &str = "MDPC01";
/// Version of MDP/Worker we implement
pub static MDPW_WORKER: &str = "MDPW01";

iota! {
    pub const MDPW_READY: u8 = iota + 1;
        | MDPW_REQUEST
        | MDPW_REPLY
        | MDPW_HEARTBEAT
        | MDPW_DISCONNECT
}

lazy_static! {
    pub static ref MDPS_COMMANDS: std::collections::HashMap<u8, &'static str> = {
        let mut map = std::collections::HashMap::new();
        map.insert(MDPW_READY, "READY");
        map.insert(MDPW_REQUEST, "REQUEST");
        map.insert(MDPW_REPLY, "REPLY");
        map.insert(MDPW_HEARTBEAT, "HEARTBEAT");
        map.insert(MDPW_DISCONNECT, "DISCONNECT");
        map
    };
}
