mod client;
//mod worker;

use super::*;

#[test]
fn has_valid_constants() {
    assert_eq!(MDPC_CLIENT, "MDPC01");
    assert_eq!(MDPW_WORKER, "MDPW01");

    assert_eq!(MDPS_COMMANDS.get(&MDPW_READY).unwrap(), &"READY");
    assert_eq!(MDPS_COMMANDS.get(&MDPW_REQUEST).unwrap(), &"REQUEST");
    assert_eq!(MDPS_COMMANDS.get(&MDPW_REPLY).unwrap(), &"REPLY");
    assert_eq!(MDPS_COMMANDS.get(&MDPW_HEARTBEAT).unwrap(), &"HEARTBEAT");
    assert_eq!(MDPS_COMMANDS.get(&MDPW_DISCONNECT).unwrap(), &"DISCONNECT");
}
