use czmq::{ZMsg, ZPoller, ZSock};
use std::error::Error;
use std::fmt;
use time::Duration;

use mdp;

/// Client struct
pub struct Client {
    /// broker name
    pub broker: String,
    /// client socket
    pub client: Option<ZSock>,
    /// polling device
    pub poller: Option<ZPoller>,
    /// heartbeat timeout
    pub timeout: Duration,
}

impl Client {
    /// The constructor and destructor are the same as in mdcliapi, except
    /// we don't do retries, so there's no retries property.
    pub fn new(broker: String) -> Result<Client, Box<dyn Error>> {
        let mut client = ZSock::new(czmq::SocketType::DEALER);
        let mut poller = ZPoller::new().unwrap();

        poller.add(&mut client).unwrap();
        client
            .connect(broker.as_str())
            .expect("failed to connect to broker");

        let c = Self {
            broker,
            client: Some(client),
            poller: Some(poller),
            timeout: Duration::milliseconds(2_500),
        };

        Ok(c)
    }

    // entire function seems pointless for now
    /*
     *    fn connect_to_broker(&mut self) -> Result<(), Box<dyn Error>> {
     *        if self.client != None {
     *            self.client
     *                .as_ref()
     *                .unwrap()
     *                .disconnect(self.broker.as_str())?;
     *            drop(self.client.as_ref().unwrap());
     *        }
     *
     *        self.client = Some(ZSock::new(czmq::SocketType::DEALER));
     *
     *        self.poller = Some(ZPoller::new().unwrap());
     *        self.poller
     *            .unwrap()
     *            .add(&mut self.client.unwrap())
     *            .unwrap();
     *
     *        self.client
     *            .unwrap()
     *            .connect(self.broker.as_str())
     *            .expect("failed to connect to broker");
     *
     *        Ok(())
     *    }
     */

    pub fn close(&mut self) -> Result<(), Box<dyn Error>> {
        if self.client != None {
            self.client
                .as_ref()
                .unwrap()
                .disconnect(self.broker.as_str())?;
            drop(self.client.as_ref().unwrap());
        }

        Ok(())
    }

    /// The send method now just sends one message, without waiting for a
    /// reply. Since we're using a DEALER socket we have to send an empty
    /// frame at the start, to create the same envelope that the REQ socket
    /// would normally make for us:
    pub fn send(&mut self, service: &str, mut request: Vec<Vec<u8>>) -> Result<(), Box<dyn Error>> {
        // Prefix request with protocol frames
        // Frame 0: empty (REQ emulation)
        // Frame 1: "MDPCxy" (six bytes, MDP/Client x.y)
        // Frame 2: Service name (printable string)
        let mut req = vec![
            b"".to_vec(),
            mdp::MDPC_CLIENT.as_bytes().to_vec(),
            service.as_bytes().to_vec(),
        ];

        req.append(&mut request);

        let msg = ZMsg::new();
        // TODO: put this into a map instead?
        for part in &req {
            msg.addbytes(&*part).unwrap();
        }
        let mut client = self.client.as_mut().unwrap();
        msg.send::<ZSock>(&mut client).unwrap();

        Ok(())
    }

    /// The recv method waits for a reply message and returns that to the
    /// caller.
    ///
    /// Returns the reply message or NULL if there was no reply. Does not
    /// attempt to recover from a broker failure, this is not possible
    /// without storing all unanswered requests and resending them all...
    pub fn recv(&mut self) -> Result<Vec<Vec<u8>>, Box<dyn Error>> {
        let poller = self.poller.as_mut().unwrap();
        let sock = poller.wait::<ZSock>(Some(self.timeout.num_milliseconds() as u32));
        let mut resp: Vec<Vec<u8>> = Vec::new();

        if let Some(mut sock) = sock {
            let msg = ZMsg::recv::<ZSock>(&mut sock).unwrap();
            sock.flush();

            // Don't attempt to handle errors, just assert
            if msg.size() < 4 {
                warn!("client: msg length < 4");
            }

            let mut num = 0;
            while let Some(bytes) = msg.popbytes().unwrap() {
                if let Some(bytes) = Some(bytes) {
                    match num {
                        0 => {
                            if std::str::from_utf8(&bytes).unwrap() != "" {
                                warn!("client: msg[0] != \"\"");
                            }
                        }
                        1 => {
                            if std::str::from_utf8(&bytes).unwrap() != mdp::MDPC_CLIENT {
                                warn!("client: msg[1] != MDPC_CLIENT");
                            }
                        }
                        2 => {} // skip the 3rd part
                        _ => resp.push(bytes.to_vec()),
                    }
                    debug!("{:?}", bytes);
                    //} else {
                    //None => warn!("Failed to receive frame bytes"),
                }

                num = num + 1;
            }
            //} else {
            //None => Err(Error::new("Client failed to receive message")),
            //Ok(());
        }

        Ok(resp)
    }
}

/// Client clean up.
impl Drop for Client {
    fn drop(&mut self) {
        self.close().expect("couldn't close client");
    }
}

/// Debug trait implementation for client
impl fmt::Debug for Client {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({:?})", self.broker)
    }
}
