# `plantd` Crate

This crate provides client, service, and message types for working with and
communicating with other components in a `plantd` network.

## Install

How can I install/​use this?

## Documentation

This file is currently the only project documentation, but as it progresses
more should be available at https://plantd.gitlab.io/plantd-rs.


## License

This library is licensed under the MIT license. For more information see the
`LICENSE` file in the root of the project.
