extern crate plantd;

use plantd::mdp::Client;

fn main() {
    let mut client = Client::new("tcp://localhost:7200".to_string()).unwrap();
    let req: Vec<Vec<u8>> = vec![
        b"get-configuration".to_vec(),
        b"{\"id\":\"0xDEADBEEF\"}".to_vec(),
    ];

    for _ in 0..1000 {
        client.send("acquire", req.clone()).unwrap();
    }

    for _ in 0..1000 {
        let resp = client.recv().unwrap();
        for msg in &resp {
            println!("{}", std::str::from_utf8(msg).unwrap());
        }
    }
}
